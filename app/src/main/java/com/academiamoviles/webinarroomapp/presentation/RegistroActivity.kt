package com.academiamoviles.webinarroomapp.presentation

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import com.academiamoviles.webinarroomapp.R
import com.academiamoviles.webinarroomapp.data.AppDatabase
import com.academiamoviles.webinarroomapp.data.Mascota
import com.academiamoviles.webinarroomapp.databinding.ActivityMascotaBinding
import com.academiamoviles.webinarroomapp.databinding.ActivityRegistroBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import java.util.concurrent.Executors

class RegistroActivity : AppCompatActivity() {

    private val EDITAR_TITULO =  "Editar Mascota"
    private val EDITAR_BOTON =  "Editar"
    private val REGISTRAR_TITULO = "Registro Mascota"
    private val REGISTRAR_BOTON =  "Registrar"

    private var codigo = 0

    private lateinit var binding: ActivityRegistroBinding
    private val appDatabase by lazy {
        AppDatabase.getInstance(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegistroBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
        events()
    }



    private fun init() {


        val bundle:Bundle? = intent.extras

        bundle?.let {

            binding.tvTitulo.text = EDITAR_TITULO
            binding.btnRegistrar.text = EDITAR_BOTON

            val mascota = it.getSerializable("key_mascota") as Mascota

            mascota?.let { _mascota ->

                binding.edtNombre.setText(_mascota.nombres)
                binding.edtRaza.setText(_mascota.raza)
                binding.edtPreferencia.setText(_mascota.preferencia)
                binding.imgMascota.setImageBitmap(convertByesToImage(mascota.imagen))
                codigo = mascota.codigo
            }

        } ?: run {

            binding.tvTitulo.text = REGISTRAR_TITULO
            binding.btnRegistrar.text = REGISTRAR_BOTON
            binding.edtNombre.setText("")
            binding.edtRaza.setText("")
            binding.edtPreferencia.setText("")
        }
    }

    private fun events() {

        binding.btnRegistrar.setOnClickListener {

            val nombres = binding.edtNombre.text.toString()
            val raza = binding.edtRaza.text.toString()
            val preferencia = binding.edtPreferencia.text.toString()

            if (binding.tvTitulo.text == REGISTRAR_TITULO){

                val mascota = Mascota(codigo, nombres, raza, preferencia,convertImageToBytes(binding.imgMascota))

                Executors.newSingleThreadExecutor().execute {

                    appDatabase?.mascotaDao()?.insert(mascota)

                    runOnUiThread {

                        mensaje("Mascota creado")
                        onBackPressed()
                    }
                }
            }
            else{

                val mascota = Mascota(codigo, nombres, raza, preferencia,convertImageToBytes(binding.imgMascota))

                Executors.newSingleThreadExecutor().execute {

                    appDatabase?.mascotaDao()?.update(mascota)

                    runOnUiThread {

                        mensaje("Mascota actualizada")
                        onBackPressed()
                    }
                }
            }


        }

        binding.imgMascota.setOnClickListener {
            solicitarPermiso()
        }
    }

    fun mensaje(mensaje: String) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show()
    }

    private fun solicitarPermiso() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                openCamera()
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    1000
                )
                return
            }
        }
    }

    fun openCamera(){
        val intent = Intent(Intent.ACTION_PICK).apply {
            type = "image/*"
        }
        startActivityForResult(intent, 2000)
    }

    fun convertImageToBytes(imageview:ImageView) : ByteArray{
        val bitmap = imageview.drawable.toBitmap()
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG,100,stream)
        return stream.toByteArray()
    }

    fun convertByesToImage(byteArray:ByteArray) : Bitmap{
        return BitmapFactory.decodeByteArray(byteArray,0,byteArray.size)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 1000) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
              openCamera()
            } else {
                mensaje("No tiene permisos para acceder")
            }
            return
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == 2000 && resultCode == Activity.RESULT_OK){
            val uri = data?.data
            val inputSteam = uri?.let { contentResolver.openInputStream(it) }
            val bitmap = BitmapFactory.decodeStream(inputSteam)
            binding.imgMascota.setImageBitmap(bitmap)

        }
        super.onActivityResult(requestCode, resultCode, data)


    }


}