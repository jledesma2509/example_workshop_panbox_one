package com.academiamoviles.webinarroomapp.presentation

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.academiamoviles.webinarroomapp.R
import com.academiamoviles.webinarroomapp.data.Mascota
import com.academiamoviles.webinarroomapp.databinding.ItemMascotasBinding

class MascotaAdapter(var mascotas:MutableList<Mascota> = mutableListOf(),
                     var callbackMascota:(mascota:Mascota)->Unit) : RecyclerView.Adapter<MascotaAdapter.MascotaAdapterViewHolder>() {

    lateinit var callbackMascotaEliminar: ((Mascota) -> Unit)


    inner class MascotaAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        private val binding : ItemMascotasBinding = ItemMascotasBinding.bind(itemView)

        fun bind(
            mascota: Mascota) = with(binding){

            tvNombre.text = mascota.nombres
            tvRaza.text = mascota.raza
            tvPreferencias.text = mascota.preferencia

            val byteArray = mascota.imagen
            val bitmap = BitmapFactory.decodeByteArray(byteArray,0,byteArray.size)
            imgMascota.setImageBitmap(bitmap)

            imgEditar.setOnClickListener {
                callbackMascota.invoke(mascota)
            }

            imgEliminar.setOnClickListener {
                callbackMascotaEliminar.invoke(mascota)
            }


        }
    }

    fun updateData(mascotas :MutableList<Mascota>){

        this.mascotas = mascotas
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MascotaAdapterViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_mascotas,parent,false)

        return MascotaAdapterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mascotas.size
    }

    override fun onBindViewHolder(holder: MascotaAdapterViewHolder, position: Int) {

        val mascota = mascotas[position]
        holder.bind(mascota)
    }
}