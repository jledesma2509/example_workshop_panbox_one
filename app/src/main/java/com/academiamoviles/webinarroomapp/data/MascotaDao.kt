package com.academiamoviles.webinarroomapp.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface MascotaDao {

    @Insert
    fun insert(mascota:Mascota)

    @Update
    fun update(mascota:Mascota)

    @Delete
    fun delete(mascota: Mascota)

    @Query("select *from tablaMascota")
    fun getAlls() : LiveData<MutableList<Mascota>>
}